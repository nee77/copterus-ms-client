//
// Results uploader
//
const FormData = require('form-data');
const axios = require("axios")
const path = require("path")
const fs = require("fs")

let HOST = "http://localhost:5009"
if(process.env.HOST) HOST = "https://" + process.env.HOST
const SETID = process.env.SETID || ""
const HS = process.env.HS || ""

if( SETID.length < 5){
    console.log("No set id, exit")
    process.exit()
}

axios.defaults.headers.common["X-Appid"] = HS

const url = HOST + "/result/" + SETID

// Загрузить файлы из папки results в хранилище
const uploadFile = (dir, file) => {
    return new Promise((resolve, reject) => {
        try {
            const fileData = fs.readFileSync(path.join(dir, file))
            const form = new FormData()
            form.append('file', fileData, file)
            console.log("Uploading " + file)
            axios.post(url, form, {headers: {...form.getHeaders()}})
                .then(res => {
                    if(res.data.status === "ok"){
                        console.log("Uploaded OK " + file)
                        resolve()
                    }
                    else {
                        console.error("FAILED TO UPLOAD (0) " + file, res.data)
                        reject(res.data)
                    }
                })
                .catch(error => {
                    console.error("FAILED TO UPLOAD (1) " + file, error)
                    reject(error)
                })
        }
        catch (error){
            console.log(error)
            reject(error)
        }

    })
}

const resultsFolder = path.join(__dirname, "images-sets", SETID, "results")
if (!fs.existsSync(resultsFolder)) {
    console.log("No result folder")
    process.exit()
}

let list = fs.readdirSync(resultsFolder)
if(list.length === 0){
    console.log("No results files")
    process.exit()
}
else {
    console.log("Uploading " + list.length + " files")
}

let uploads = list.map(file => uploadFile(resultsFolder, file))
Promise.allSettled(uploads)
    .then(results => {
        let oks = 0
        let errors = 0
        results.forEach(result => {
            if (result.status === "fulfilled") oks++
            else errors++
        })
        if( errors > 0){
            console.log("ERRORS", errors)
        }
        else {
            console.log("All files uploaded OK")
        }
    })

