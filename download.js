//
// Original images downloader
//
const axios = require("axios")
const path = require("path")
const fs = require("fs")

let HOST = "http://localhost:5009"
if(process.env.HOST) HOST = "https://" + process.env.HOST
const SETID = process.env.SETID || ""
const HS = process.env.HS || ""

if(SETID.length < 5){
    console.log("No set id, exit")
    process.exit()
}

axios.defaults.headers.common["X-Appid"] = HS

const saveDir = path.join(__dirname, "images-sets")
const downloadingRootDir = path.join(saveDir, "_downloading-"+SETID)
const downloadingImagesDir = path.join(downloadingRootDir, "originals")
const finalDir = path.join(saveDir, SETID)
const resultsDir = path.join(finalDir, "results")

if (!fs.existsSync(downloadingImagesDir)) {
    console.log("Creating directory")
    fs.mkdirSync(downloadingImagesDir, {recursive: true})
}

async function downloadImage(url, localFile) {
    const writer = fs.createWriteStream(localFile)
    console.log("Downloading " + url)
    const response = await axios({url, method: 'GET', responseType: 'stream'})

    console.log("Saving " + localFile)
    response.data.pipe(writer)

    return new Promise((resolve, reject) => {
        writer.on('finish', resolve)
        writer.on('error', reject)
    })
}

axios.get(HOST + "/image-set/" + SETID)
    .then(res => {
        let names = Array.isArray(res.data) ? res.data : []
        if(names.length === 0){
            console.log("Empty list, nothing to download")
            return
        }

        console.log("Received list of " + names.length + " files")

        let downloads = names.map(fn => downloadImage(HOST + "/image-set/" + SETID + "/" + fn, path.join(downloadingImagesDir, fn)))

        Promise.allSettled(downloads)
            .then(results => {
                let oks = 0
                let errors = 0
                results.forEach(result => {
                    if (result.status === "fulfilled") oks++
                    else errors++
                })
                if( errors > 0){
                    console.log("ERRORS", errors)
                }
                else {
                    console.log("All files downloaded OK")
                    try {
                        // Удалить старую папку если она есть
                        // if (fs.existsSync(finalDir)){ fs.rmSync(finalDir, { recursive: true }) }
                        fs.renameSync(downloadingRootDir, finalDir)
                        console.log("Directory renamed")
                        if (!fs.existsSync(resultsDir)) {
                            console.log("Creating results directory")
                            fs.mkdirSync(resultsDir, {recursive: true})
                        }
                    } catch(err) {
                        console.log(err)
                    }
                }
            })
    })
    .catch( err => {
        console.log(err)
    })

